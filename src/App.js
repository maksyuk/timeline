import React, { Component } from 'react';
import Timeline from './containers/Timeline';
import './App.css';
import 'antd/dist/antd.css';

class App extends Component {

  render() {
    return (
      <div className="App">
          <Timeline />
      </div>
    );
  }
}

export default App;
