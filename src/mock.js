import moment from "moment";

export const events = [
    {
        dateStart: moment("2018-09-1", "YYYY-MM-DD"),
        title: "event 1",
        dateEnd: moment("2018-09-3", "YYYY-MM-DD"),
        id: 1,
        level: 0
    },
    {
        dateStart: moment("2018-01-1", "YYYY-MM-DD"),
        title: "event 2",
        dateEnd: moment("2018-01-3", "YYYY-MM-DD"),
        id: 4,
        level: 0
    },
    {
        dateStart: moment("2018-01-1", "YYYY-MM-DD"),
        title: "event 21",
        dateEnd: moment("2018-01-3", "YYYY-MM-DD"),
        id: 41,
        level: 0
    },
    {
        dateStart: moment("2018-01-6", "YYYY-MM-DD"),
        title: "event 3",
        dateEnd: moment("2018-01-9", "YYYY-MM-DD"),
        id: 5,
        level: 0
    },
    {
        dateStart: moment("2018-09-6", "YYYY-MM-DD"),
        title: "event 4",
        dateEnd: moment("2018-09-7", "YYYY-MM-DD"),
        id: 2,
        level: 0
    }
];
export const states = [
    {
        dateStart: moment("2018-09-1", "YYYY-MM-DD"),
        title: "state 1",
        dateEnd: moment("2018-09-3", "YYYY-MM-DD"),
        id: 5
    },
    {
        dateStart: moment("2018-01-1", "YYYY-MM-DD"),
        title: "state 4",
        dateEnd: moment("2018-01-9", "YYYY-MM-DD"),
        id: 1
    },
    {
        dateStart: moment("2018-09-4", "YYYY-MM-DD"),
        title: "state 2",
        dateEnd: moment("2018-09-9", "YYYY-MM-DD"),
        id: 2
    }
];