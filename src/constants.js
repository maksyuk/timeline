import moment from 'moment';
export const EMPTY_EVENT = {
    dateStart: moment(),
    title: "",
    dateEnd: moment(),
    id:0
};

export const EMPTY_STATE = {
    dateStart: moment(),
    title: "",
    dateEnd: moment(),
    id:0
};