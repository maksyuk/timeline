import React from "react";
import Grid from "../components/grid/Grid";
import ControllForm from "../components/control-form/ControllForm";
import { getRange, sortedByDateStart, calcLevel } from "../utils";
import {states, events} from "../mock";

const range = getRange("year");

class Timeline extends React.Component {
  constructor() {
      super();
      this.state = {
          range,
          events: calcLevel(events),
          states,
          activeEvent: null,
          activeState: null,
          zoom: 0,
      };
  }


  onSaveEvent = ev =>
    this.setState({
      events: calcLevel(sortedByDateStart([
        ...this.state.events,
        {
          ...ev,
          id: +new Date(),
        }
      ])),
      activeEvent: null
    });

    onSaveState = ev =>
        this.setState({
            states: [
                ...this.state.states,
                {
                    ...ev,
                    id: +new Date(),
                }
            ],
            activeState: null
        });

  onActiveEvent = (activeEvent) => {
    this.setState({activeEvent, activeState: null});
  }

    onActiveState = (activeState) => {
        this.setState({activeState, activeEvent: null});
    }

  onSaveChangeEvent = (event) => {
    const events = this.state.events.map((ev) => {
        if(ev.id === event.id) return event;
          return ev;
    });
      this.setState({events, activeEvent: null});
  };
    onSaveChangeState = (state) => {
        const states = this.state.states.map((st) => {
            if(st.id === state.id) return state;
            return st;
        });
        this.setState({states, activeState: null});
    };

  render() {
    const { range, events, states, activeEvent, activeState } = this.state;

    return (
      <div className='timeline'>
        <Grid
          events={events}
          states={states}
          range={range}
          onChangeEvent={this.onActiveEvent}
          onChangeState={this.onActiveState}
        />
        <ControllForm
            onSaveEvent={this.onSaveEvent}
            onSaveChangeEvent={this.onSaveChangeEvent}
            onSaveState={this.onSaveState}
            onSaveChangeState={this.onSaveChangeState}
            activeEvent={activeEvent}
            activeState={activeState}
            onActiveEvent={this.onActiveEvent}
            onActiveState={this.onActiveState}
        />
      </div>
    );
  }
}

export default Timeline;
