import moment from "moment";

export const getRange = period => {
  //period = month
  const startOfWeek = moment().startOf(period);
  const endOfWeek = moment().endOf(period);

  let days = [];
  let day = startOfWeek;

  while (day <= endOfWeek) {
    days.push(day.toDate());
    day = day.clone().add(1, "d");
  }
  return days;
};

export const calcOffset = (start, end) => {
  const msInDay = 86400000;
  const m = end - start;
  return (m / msInDay) * 70; // 70 - width col
};

export const getPosition = (
  { dateStart, dateEnd, level = 0 },
  start,
  color = "#8b8bd8"
) => {
  const left = calcOffset(start.valueOf(), dateStart.valueOf());
  const width = calcOffset(dateStart.valueOf(), dateEnd.valueOf());
  return { left, width, background: color, bottom: level * 50 };
};

export const sortedByDateStart = arr => {
  return arr.sort((a, b) => a.dateStart > b.dateStart);
};

export const calcLevel = listEvents => {
    return listEvents.map((ev, ind) => {
        for (let i = ind; i > 0; i--) {
            if (listEvents[i - 1].dateEnd.valueOf() >= ev.dateStart.valueOf()) {
                ev.level = listEvents[i - 1].level + 1;
                return ev;
            }
        }
        return ev;
    });
};

export const setTimeZero = (time) => time.set({'hour': 0, 'minute': 0, 'second': 0, 'millisecond': 0});

export const getListYers= () => [
    1990, 1991, 1992, 1993, 1994, 1995,
    1996, 1997, 1998, 1999, 2000, 2001,
    2002, 2003, 2004, 2005, 2006, 2007,
    2008, 2009, 2010, 2011, 2012, 2013,
    2014, 2015, 2016, 2017, 2018, 2019
];