import React from "react";
import moment from "moment";
import { getPosition } from "../../utils";
import "./grid.css";

class Grid extends React.Component {
    wrapGrid = React.createRef();
    componentDidMount() {
        // console.log(13)
        // this.offsetX = this.wrapGrid.current.offsetWidth;
        // this.offsetY = this.wrapGrid.current.offsetHeight;
        // this.wrapGrid.current.addEventListener('wheel', (e) => {
        //    const z = e.deltaY < 0 ? -0.1: 0.1;
        //     this.setState({
        //         zoom: this.state.zoom + z
        //     })
        //   console.log(e);
        //     this.offsetX += e.deltaX;
        //     this.offsetY += e.deltaY;
        //     this.wrapGrid.current.scrollTo(this.offsetX, this.offsetY)
        // })
        // console.log(this.wrapGrid.current.style.height)
        this.wrapGrid.current.scrollTo(0, this.wrapGrid.current.offsetHeight)
    }

  render() {
    const { events, range, states, onChangeEvent, onChangeState } = this.props;
    const start = range[0];
    return (
      <div className="wrap-grid" ref={this.wrapGrid}>
        <div className="grid">
          <div className="event-line">
            {events.map(ev => (
              <div className="event" onClick={() => onChangeEvent(ev)} key={ev.id} style={getPosition(ev, start)}>
                {ev.title}
              </div>
            ))}
          </div>
          <div className="date-line">
            {range.map((date, ind) => (
              <div className="col" key={+date}>
                {date.getDate()}
                {ind % 10 === 0 && (
                  <div className="month" key={`month_${+date}`}>
                    {moment(date).format("MMMM YYYY")}
                  </div>
                )}
              </div>
            ))}
          </div>

          <div className="stage-line">
            {states.map(state => (
              <div
                className="stage"
                key={state.id}
                style={getPosition(state, start, "gold")}
                onClick={() => onChangeState(state)}
              >
                {state.title}
              </div>
            ))}
          </div>
        </div>
      </div>
    );
  }
}

export default Grid;
