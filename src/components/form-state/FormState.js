import React from "react";
import { Input, DatePicker, Form, Button } from "antd";
import "./form-state.css";
import { setTimeZero } from "../../utils";

class FormState extends React.Component {
  state = {
    ...this.props.activeState,
    isNewState: this.props.activeState.id === 0,
    staticTitle: this.props.activeState.title
  };

  onChange = type => e => this.setState({ [type]: e.target.value });

  onChangeDate = dateList => {
    this.setState({
      dateStart: setTimeZero(dateList[0]),
      dateEnd: setTimeZero(dateList[1])
    });
  };

  onChangeTitle = this.onChange("title");

  onCancel = () => this.props.onActiveState(null);

  onSubmit = e => {
    e.preventDefault();
    const { title, dateEnd, dateStart, id } = this.state;
    this.props.form.validateFields(err => {
      if (!err) this.props.onSaveState({ title, dateEnd, dateStart, id });
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { dateStart, dateEnd, isNewState, title, staticTitle } = this.state;
    const valueTime = [dateStart, dateEnd];
    return (
      <div className="form-event">
        <h2>
          State Description{" "}
          {!isNewState && <div className="state-in-form">{staticTitle}</div>}
        </h2>
        <Form onSubmit={this.onSubmit}>
          <Form.Item>
            {getFieldDecorator("title", {
              rules: [{ required: isNewState, message: "Please input title!" }]
            })(
              <div>
                <label htmlFor="title">Short title</label>
                <Input
                  id="title"
                  onChange={this.onChangeTitle}
                  value={title}
                  type="text"
                />
              </div>
            )}
          </Form.Item>
          <Form.Item>
            <DatePicker.RangePicker
              value={valueTime}
              id="formDatepicker"
              onChange={this.onChangeDate}
              size={1}
            />
          </Form.Item>
          <div>
            <Button
              type="primary"
              style={{ background: "#4CAF50", borderColor: "#4CAF50" }}
              size="default"
              htmlType="submit"
            >
              Save
            </Button>
            <Button
              type="primary"
              style={{
                background: "#FF5722",
                borderColor: "#FF5722",
                marginLeft: "20px"
              }}
              size="default"
              onClick={this.onCancel}
            >
              Cancel
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

export default Form.create()(FormState);
