import React from "react";
import { Input, DatePicker, Button, Form } from "antd";
import "./form-event.css";
import { setTimeZero } from "../../utils";

class FormEvent extends React.Component {
  state = {
    ...this.props.activeEvent,
    isNewEvent: this.props.activeEvent.id === 0
  };
  onChange = type => e => this.setState({ [type]: e.target.value });
  onChangeDate = dateList => {
    this.setState({
      dateStart: setTimeZero(dateList[0]),
      dateEnd: setTimeZero(dateList[1])
    });
  };
  onChangeTitle = this.onChange("title");

  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields(err => {
      const { title, dateEnd, dateStart, id } = this.state;
      if (!err) this.props.onSaveEvent({ title, dateEnd, dateStart, id });
    });
  };

  render() {
    const { dateStart, dateEnd, isNewEvent, title } = this.state;
    const { getFieldDecorator } = this.props.form;
    const valueTime = [dateStart, dateEnd];
    return (
      <div className="form-event">
        <h2>
          Event Description{" "}
          {!isNewEvent && <div className="event-in-form">{title}</div>}
        </h2>
        <Form onSubmit={this.onSubmit}>
          <Form.Item>
            {getFieldDecorator("title", {
              rules: [
                {
                  required: isNewEvent,
                  message: "Please input title!"
                }
              ]
            })(
              <div>
                <label htmlFor="title">Short title</label>
                <br />
                <Input
                  id="title"
                  onChange={this.onChangeTitle}
                  value={title}
                  type="text"
                />
              </div>
            )}
          </Form.Item>
          <Form.Item>
            <DatePicker.RangePicker
              value={valueTime}
              onChange={this.onChangeDate}
              size={1}
            />
          </Form.Item>
          <div>
            <Button
              type="primary"
              style={{ background: "#4CAF50", borderColor: "#4CAF50" }}
              size="default"
              htmlType="submit"
            >
              Save
            </Button>
            <Button
              type="primary"
              style={{
                background: "#FF5722",
                borderColor: "#FF5722",
                marginLeft: "20px"
              }}
              size="default"
              onClick={() => this.props.onActiveEvent(null)}
            >
              Cancel
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

export default Form.create()(FormEvent);
