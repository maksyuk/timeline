import React from "react";
import { Button } from "antd";
import FormEvent from "../form-event/FormEvent";
import FormState from "../form-state/FormState";
import { EMPTY_EVENT, EMPTY_STATE } from "../../constants";
import "./control-form.css";

class ControllForm extends React.Component {
  createNewEvent = () => {
    this.props.onActiveEvent({ ...EMPTY_EVENT });
  };

  createNewState = () => {
    this.props.onActiveState({ ...EMPTY_STATE });
  };

  checkSaveFuncEvent = () => {
    return this.props.activeEvent && this.props.activeEvent.id === 0
      ? this.props.onSaveEvent
      : this.props.onSaveChangeEvent;
  };

  checkSaveFuncState = () => {
    return this.props.activeState && this.props.activeState.id === 0
      ? this.props.onSaveState
      : this.props.onSaveChangeState;
  };

  render() {
    const onSaveEvent = this.checkSaveFuncEvent();
    const onSaveState = this.checkSaveFuncState();

    return (
      <div className="control-form">
        <div className="control-form__button-group">
          <Button
            type="primary"
            className="button-group__add-event"
            onClick={this.createNewEvent}
            size="default"
          >
            Add event
          </Button>
          <Button
            type="primary"
            className="button-group__add-state"
            onClick={this.createNewState}
            size="default"
          >
            Add state
          </Button>
        </div>
        {this.props.activeEvent && <FormEvent {...this.props} onSaveEvent={onSaveEvent} />}
        {this.props.activeState && <FormState {...this.props} onSaveState={onSaveState} />}
      </div>
    );
  }
}

export default ControllForm;
